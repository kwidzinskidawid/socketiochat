/* jshint node: true */
var express = require('express');
var app = express();
var httpServer = require('http').Server(app);
var io = require('socket.io')(httpServer);
var path = require('path');
var _ = require('underscore');
var moment = require('moment');
var less = require('less-middleware');

var port = process.env.PORT || 3000;

var messages = [
    {
        room: 'Main Room',
        msgs: []
    }
];

var connectedUsers = [];
var rooms = ['Main Room', 'Fun', 'Science', 'AFK'];

app.use('/lib/', express.static(__dirname + '/bower_components/jquery/dist/'));
app.use('/lib/', express.static(__dirname + '/bower_components/underscore/'));

app.use(less(path.join(__dirname, '/public')));
app.use(express.static(path.join(__dirname, '/public')));

io.sockets.on('connection', function (socket) {

    socket.on('tryLogin', function(data) {
        //checking if there isnt a user connected with same name
        if (_.indexOf(connectedUsers, data) === -1) {
            
            socket.join('Main Room');
            socket.room = 'Main Room';
            socket.name = data;

            socket.emit('loginOk', socket.room);
            socket.emit('sendRoomList', rooms);

            socket.emit('messageFromServer', "You have connected to chat."); 
            if(messages[0].msgs.length > 0) {
                socket.emit('sendArchives', messages[0].msgs);
            }
            socket.emit('messageFromServer', "Hi " + socket.name + "!");

            connectedUsers.push(socket.name);

            var msg = socket.name + ' joined the chat.';
            socket.broadcast.to(socket.room).emit('messageFromServer', msg);

        } else {
            socket.emit('loginError', "User with this nickname is already logged in.");
        }

    });

    //default message event with archiving
    socket.on('messageFromClient', function (data) {
       var time = "[" + moment().format('h:mma') + "] ";
       
       archiveMessages(data, socket.room, socket.name, time);
       io.in(socket.room).emit('messageFromServer', data, socket.name, time);
    });

    socket.on('changeRoom', function(data) {
        //adding new room
        if (_.indexOf(rooms, data) === -1) {
            rooms.push(data);
            socket.emit('sendRoomList', rooms);
        }

        //leaving room
        var oldRoom = socket.room;
        socket.leave(oldRoom);
        var msg = socket.name +' left this room.';
        socket.broadcast.to(oldRoom).emit('messageFromServer', msg);

        //joining room
        socket.join(data);
        socket.room = data;
        var msg = socket.name + ' joined this room.';
        socket.broadcast.to(data).emit('messageFromServer', msg);

        socket.emit('changeOk', data);
        var msg = "You are now in: " + data + ".";
        socket.emit('messageFromServer', msg);


        //getting archives
        var roomIndex = _.findIndex(messages, function(el) {
            return el.room === socket.room;
        });
        if(roomIndex !== -1 && messages[roomIndex].msgs.length > 0) {
            socket.emit('sendArchives', messages[roomIndex].msgs);
        }
    });


    socket.on('error', function (err) {
        console.dir(err);
    });

    socket.on('disconnect', function () {
        //tell others about disconnecting
        if ( socket.name !== undefined) {
            var msg = socket.name +' left chat.';
            socket.broadcast.to(socket.room).emit('messageFromServer', msg);
            connectedUsers = _.without(connectedUsers, socket.name);
        }

    });
});


//archive function
var archiveMessages = function(msg, room, name, time) {
    //check if room exists in the archives
    var roomIndex = _.findIndex(messages, function(el) {
        return el.room === room;
    });

    //if not create the room
    if(roomIndex === -1) {
        messages.push({room: room, msgs: []});
        roomIndex = messages.length - 1;

    //archive only 20 messages, if more, delete oldest
    } else if ( messages[roomIndex].msgs.length >= 20) {
        messages[roomIndex].msgs = _.rest(messages[roomIndex].msgs);
    }
    
    //actual archiving
    messages[roomIndex].msgs.push({time: time, name: name, data: msg});
    
};



httpServer.listen(port, function () {
    console.log('HTTP Server listening on port  ' + port);
});

/* jshint browser: true, globalstrict: true, devel: true */
/* global io: false */
"use strict";

$().ready(function() {
    var socket;

    if (!socket || !socket.connected) {
            socket = io({forceNew: true});
        }

    socket.on('connect', function () {
            console.log('Successfully connected using Socket.io');
        });

    socket.on('error', function (err) {
            $('#login .errorMsg').text("Błąd połączenia z serwerem: '" + JSON.stringify(err) + "'")
        });

    $('#chat').hide();

    $('#name').focus();
    $('#name').keyup(function(event){
        if(event.keyCode == 13){
            $('#join').click();
        }
    });

    $('#join').click(function() {
        if ($('#name').val() === "") {
            $('#name').addClass("required");
            $('#login .errorMsg').text('Please choose your name.');
            return;
        }
        $('#name').removeClass('required');
        
        socket.emit('tryLogin', $('#name').val());
        
    }); 

    socket.on('loginError', function(data) {
        $('#login .errorMsg').text(data);
     });

    socket.on('loginOk', function(data) {
        $('#login').hide();
        $('#chat').show();

        $('#roomName').text(data);
        $('#text').focus();

        //update available rooms
        socket.on('sendRoomList', function(data) {
            $('.roomList').html('');
            _.each(data, function(el) {
                $('.roomList').append('<p class="room">' + el + "</p>");
            });
        });

        //room change accepted
        socket.on('changeOk', function(data) {
            $('#roomName').text(data);
            $('#messagesArea').html('');
            $('#text').focus();
        });

        //get archived messages
        socket.on('sendArchives', function(data) {
            _.each(data, function(el) {
                var msg = "<p>" + "<span class='time'>" + el.time +
                 "</span>" + "<span class='name'>" + el.name + "</span>" + ': ' + el.data + "</p>";
                $('#messagesArea').append(msg);
            });

            $('#messagesArea').scrollTop($('#messagesArea')[0].scrollHeight);
        });


        //default message event (get broadcasts)
        socket.on('messageFromServer', function (data, name, time) {
            if(name !== undefined && time !== undefined) {
                var msg = "<p>" + "<span class='time'>" + time +
                 "</span>" + "<span class='name'>" + name + "</span>" + ': ' + data + "</p>";
            } else {
                var msg = "<p class='serverMessage'>" + data + "</p>";
            }
            
            $('#messagesArea').append(msg);
            $('#messagesArea').scrollTop($('#messagesArea')[0].scrollHeight);

        });

    });

   //send message to server
    $('#send').click(function() {
        socket.emit('messageFromClient', $('#text').val());

        $('#text').val("");
        $('#text').focus();
    });

    $('#text').keyup(function(event){
        if(event.keyCode == 13 && $('#text').val() !== ''){
            $('#send').click();
        }
    });

///////////////////////////////////////////////
//ROOM CHANGER

    $('#changeRoom').click(function() {
        $('.popupWrapper').fadeIn();
    });

    $('.close').click(function() {
        $('.popupWrapper').fadeOut();
    });

    //choosing new room
   $('.roomList').on('click', '.room', function() {
        socket.emit('changeRoom', $(this).text());
        $('.popupWrapper').fadeOut();
    });


    $('#newRoomName').keyup(function(event){
        if(event.keyCode == 13){
            $('#create').click();
        }
    });

   $('#create').click(function() {
        if ($('#newRoomName').val() === "") {
            $('#newRoomName').addClass("required");
            $('#errorMsg').text("Enter new room's name");
            return;
        }
        $('#newRoomName').removeClass("required");
        $('#popup .errorMsg').text('');
        
        socket.emit('changeRoom', $('#newRoomName').val());

        $('.popupWrapper').fadeOut();
        $('#newRoomName').val('');
   });


});
    

